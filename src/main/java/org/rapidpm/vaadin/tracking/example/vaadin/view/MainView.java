package org.rapidpm.vaadin.tracking.example.vaadin.view;

import com.vaadin.navigator.View;
import com.vaadin.ui.Composite;
import com.vaadin.ui.Label;

public class MainView extends Composite implements View {
  public static final String URL = "main";

  public MainView() {
    setCompositionRoot(new Label("This is the main view"));
  }
}
