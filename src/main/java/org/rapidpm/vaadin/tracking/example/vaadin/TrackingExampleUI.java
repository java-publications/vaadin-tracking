package org.rapidpm.vaadin.tracking.example.vaadin;

import org.rapidpm.vaadin.tracking.example.vaadin.view.MainView;
import org.rapidpm.vaadin.tracking.example.vaadin.view.StartView;
import org.rapidpm.vaadin.tracking.example.vaadin.views.ErrorView;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

public class TrackingExampleUI extends UI {
  Navigator navigator;


  @Override
  protected void init(VaadinRequest request) {
    getPage().setTitle("Navigation Example");

    // Create a navigator to control the views
    navigator = new Navigator(this, this);
    navigator.setErrorProvider(new ViewProvider() {

      @Override
      public String getViewName(String viewAndParameters) {
        return viewAndParameters;
      }

      @Override
      public View getView(String viewName) {
        return new ErrorView(viewName);
      }
    });
    // Create and register the views
    navigator.addView(StartView.URL, new StartView());
    navigator.addView(MainView.URL, new MainView());
  }
}
