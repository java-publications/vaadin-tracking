package org.rapidpm.vaadin.tracking.example.vaadin;

import javax.servlet.annotation.WebServlet;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

@WebServlet("/*")
@VaadinServletConfiguration(productionMode = false, ui = TrackingExampleUI.class)
public class TrackingExampleServlet extends VaadinServlet {

}
