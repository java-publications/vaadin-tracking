package org.rapidpm.vaadin.tracking.example;

import static io.undertow.Handlers.redirect;
import static io.undertow.servlet.Servlets.servlet;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import org.rapidpm.vaadin.tracking.example.vaadin.TrackingExampleServlet;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.Undertow.ListenerInfo;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;

public class Application {

  private Application() {
    throw new IllegalAccessError("Main class");
  }

  private static final Logger LOGGER = Logger.getLogger(Application.class.getName());
  public static final String CONTEXT_PATH = "/";

  public static void main(String[] args) throws ServletException {
    DeploymentInfo servletBuilder = Servlets.deployment()
        .setClassLoader(Application.class.getClassLoader()).setContextPath(CONTEXT_PATH)
        .setDeploymentName("ROOT.war").setDefaultEncoding("UTF-8").addServlets(
            servlet(TrackingExampleServlet.class.getSimpleName(), TrackingExampleServlet.class)
                .addMapping("/*"));

    DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);

    manager.deploy();

    PathHandler path =
        Handlers.path(redirect(CONTEXT_PATH)).addPrefixPath(CONTEXT_PATH, manager.start());

    Undertow undertowServer =
        Undertow.builder().addHttpListener(8080, "0.0.0.0").setHandler(path).build();
    undertowServer.start();
    List<ListenerInfo> listenerInfos = undertowServer.getListenerInfo();
    for (ListenerInfo listenerInfo : listenerInfos) {
      String info = "ListenerInfo{" + "protcol='" + listenerInfo.getProtcol() + '\'' + ", address="
          + listenerInfo.getAddress() + '}';
      LOGGER.info(info);
    }

  }
}
