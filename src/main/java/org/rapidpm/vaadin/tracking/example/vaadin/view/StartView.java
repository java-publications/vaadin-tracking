package org.rapidpm.vaadin.tracking.example.vaadin.view;

import com.vaadin.navigator.View;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class StartView extends VerticalLayout implements View {
  public static final String URL = "";

  public StartView() {
    setSizeFull();

    Button button = new Button("Go to Main View",
        event -> UI.getCurrent().getNavigator().navigateTo(MainView.URL));

    addComponent(button);
    setComponentAlignment(button, Alignment.MIDDLE_CENTER);
  }
}
