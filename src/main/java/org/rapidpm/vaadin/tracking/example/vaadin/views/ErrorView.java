package org.rapidpm.vaadin.tracking.example.vaadin.views;

import org.rapidpm.vaadin.tracking.example.vaadin.view.MainView;
import com.vaadin.navigator.View;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class ErrorView extends VerticalLayout implements View {

  public ErrorView(String viewName) {
    setSizeFull();
    Label message = new Label("The view " + viewName + " does not exist");
    Button button = new Button("Go to Main View",
        event -> UI.getCurrent().getNavigator().navigateTo(MainView.URL));

    addComponents(message, button);
    setComponentAlignment(button, Alignment.MIDDLE_CENTER);
  }

}
