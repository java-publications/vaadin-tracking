Tracking mit Vaadin
===================

Wo können wir tracken
---------------------

  * Frontend (JavaScript, Addblocker)
  * Backend - In Vaadin als Serverseitiges Framework gut möglich
  

Wie können wir tracken
----------------------

  * GA (Datenschutz?)
  * Logger
  * Datenbank
  * Trackingsystem (piwik)
  

Wo können wir uns einhängen
---------------------------

  * Navigator ViewChangeListener
  * Listener (ClickListener für Details, Bestellung, etc.)

Was können wir tracken
----------------------

  * IP
  * angemeldeter Nutzer
  * A oder B bei A/B Tests (evtl. mit Togglz?)
  * ...
  

Sonstiges
---------

  * Piwik mit docker starten für Beispiele
  * Piwik Vaadin Addon